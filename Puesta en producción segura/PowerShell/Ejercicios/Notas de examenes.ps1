﻿# Saber si un numero metido por teclado es par o impar.
# Se visualizará la calificación cuando la nota tecleada sea correcta (entre 0 y 10)

cls
[float] $nota=Read-Host "Teclea la nota del examen"



if ($nota -lt 0 -or $nota -gt 10) {


    if ($nota -lt 5) {

        $resultado="Suspenso"

    } else{

        $resultado="Aprobado"

            if ($nota -ge 6) {

                $resultado="bien"

            }

            if ($nota -ge 7 -and $nota -le 8) {

                $resultado="notable"

            }

            if ($nota -ge 9 -and $nota -le 10) {

                $resultado="sobresaliente"
    
            }
   }

}else{

            $resultado="errónea"
}

Write-Host "El resultado es " $resultado